# Use the official lightweight Node.js 12 image.
# https://hub.docker.com/_/node
FROM node:12-alpine

# Create and change to the app/frontend directory.
WORKDIR /usr/src/app/frontend

# Copy application dependency manifests to the container image.
# A wildcard is used to ensure both package.json AND package-lock.json are copied.
# Copying this separately prevents re-running npm install on every code change.
COPY frontend/package.json ./
COPY frontend/yarn.lock ./

# Install dependencies.
RUN yarn install

# Create and change to the app/backend directory.
WORKDIR /usr/src/app/backend

# Copy application dependency manifests to the container image.
# A wildcard is used to ensure both package.json AND package-lock.json are copied.
# Copying this separately prevents re-running npm install on every code change.
COPY backend/package.json ./
COPY backend/yarn.lock ./

# Install dependencies.
RUN yarn install

# Create and change to the app directory.
WORKDIR /usr/src/app

# Copy local code to the container image.
COPY . ./

# Change to the app/frontend directory.
WORKDIR /usr/src/app/frontend

# Build the frontend
RUN yarn build

# Change to the app/backend directory.
WORKDIR /usr/src/app/backend

# Compile backend
RUN yarn compile

# Run the web service on container startup.
CMD [ "yarn", "run", "startnocompile" ]
